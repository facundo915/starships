package edu.austral.starship;

import edu.austral.starship.model.Ship;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class KeyBindings {
    private Map<Integer, Consumer<Ship>> keys;

    public KeyBindings(){
        keys = new HashMap<>();
    }

    public static KeyBindings def() {
        KeyBindings def = new KeyBindings();
        def.bindKey(KeyEvent.VK_W,Ship.accelerate());
        def.bindKey(KeyEvent.VK_S,Ship.decelerate());
        def.bindKey(KeyEvent.VK_A,Ship.rotCounterClockwise());
        def.bindKey(KeyEvent.VK_D,Ship.rotClockWise());
        def.bindKey(KeyEvent.VK_SPACE,Ship.shoot());
        return def;
    }

    public void unbindKey(Integer keyCode){
        this.keys.remove(keyCode);
    }

    public void bindKey(Integer keyCode, Consumer<Ship> function){
        this.keys.put(keyCode,function);
    }

    public Consumer<Ship> getFuncitonForKeyCode(Integer keyCode){
        return keys.get(keyCode);
    }
}
