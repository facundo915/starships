package edu.austral.starship;

public enum Event {
    CREATED, DAMAGE, DESTROYED, DESTROYED_SHIP, DESTROYED_ASTEROID
}
