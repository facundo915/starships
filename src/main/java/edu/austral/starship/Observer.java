package edu.austral.starship;

public interface Observer<T> {
    public void update(T t, Event event);
}
