package edu.austral.starship.base.vector;

import java.awt.geom.AffineTransform;

import static java.lang.Math.*;

public class Vector2 {
    private final float x;
    private final float y;

    public Vector2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vector2 add(Vector2 other) {return new Vector2(x + other.x, y + other.y);}

    public Vector2 substract(Vector2 other) {return new Vector2(x - other.x, y - other.y);}

    public Vector2 multiply(Float scalar) {return new Vector2(x * scalar, y * scalar);}

    public Vector2 rotate(float angle) {
        return new Vector2((float) (x * cos(angle) - y * sin(angle)), (float) (x * sin(angle) + y * cos(angle)));
    }

    public float module() { return (float) Math.pow(Math.pow(x, 2) + Math.pow(y, 2), 0.5);}

    public Vector2 unitary() {
        final float module = module();
        return new Vector2(x / module, y / module);
    }

    //Angle for rotating images in processing (clockwise positive)
    public float angle() {return (float) (atan2(y, x) - atan2(1, 0));}

    public static Vector2 vector(float x, float y) {return new Vector2(x, y);}

    //Reminder: rotation is inverse of processing's rotation (i think)
    public static Vector2 vectorFromModule(float module, float angle) {
        return new Vector2((float) (module * cos(angle)), (float) (module * sin(angle)));
    }

    public AffineTransform getTransform(){
        return AffineTransform.getTranslateInstance(-x,-y);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    @Override
    public String toString(){
        return "{ " + this.x + " ; " + this.y + " }";
    }
}
