package edu.austral.starship.view;

import processing.core.PImage;

public class TexturePack {
    private PImage shipSprite;
    private PImage projectileSprite;
    private PImage obstacleSprite;

    public TexturePack(PImage shipSprite, PImage projectileSprite, PImage obstacleSprite) {
        this.shipSprite = shipSprite;
        this.projectileSprite = projectileSprite;
        this.obstacleSprite = obstacleSprite;
    }

    public PImage getShipSprite() {
        return shipSprite;
    }

    public PImage getProjectileSprite() {
        return projectileSprite;
    }

    public PImage getObstacleSprite() {
        return obstacleSprite;
    }

}
