package edu.austral.starship.view;

import edu.austral.starship.Player;
import edu.austral.starship.base.vector.Vector2;
import processing.core.PGraphics;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class HUD {

    private static final int PADDING = 20;
    private final List<Player> players;
    private final int screenW;
    private final int screenH;

    public HUD( int screenW, int screenH) {
        this.players = new ArrayList<>();
        this.screenW = screenW;
        this.screenH = screenH;
    }

    public void draw(PGraphics graphics) {
        int x = (int) (screenW * 0.05);
        int y = (int) (screenH * 0.90);
        players.sort(Comparator.comparing(p -> p.getNumber()));
        for(Player p : players){
            graphics.textSize(14);
            graphics.text("Player " + p.getNumber(),x,y);
            graphics.textSize(10);
            x += 10;
            y += PADDING;
            graphics.text("Score: " + p.getScore(),x,y);
            y += PADDING;
            if(p.getLives() > 0)
                graphics.text("Lives: " + p.getLives(),x,y);
            else {
                graphics.text("ELIMINATED", x, y);
                graphics.text("Press F" + p.getNumber() +" to respawn", x, y + 10);
            }
            x += 100;
            y -= 2 * PADDING;
        }
    }
    public void addPlayer(Player p){
        this.players.add(p);
    }

    public void removePlayer(Player p){
        this.players.remove(p);
    }
}
