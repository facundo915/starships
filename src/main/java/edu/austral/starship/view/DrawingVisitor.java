package edu.austral.starship.view;

import edu.austral.starship.Visitor;
import edu.austral.starship.model.Actor;
import edu.austral.starship.model.Obstacle;
import edu.austral.starship.model.Projectile;
import edu.austral.starship.model.Ship;
import processing.core.PConstants;
import processing.core.PGraphics;
import processing.core.PImage;

import java.awt.geom.Rectangle2D;
import java.util.List;

public class DrawingVisitor implements Visitor {
    private PGraphics graphics;
    private TexturePack textures;
    private boolean drawHitboxes;
    private int[] tints = new int[4];

    public DrawingVisitor(PGraphics graphics, TexturePack textures, boolean drawHitboxes) {
        this.graphics = graphics;
        this.textures = textures;
        this.drawHitboxes = drawHitboxes;
        tints[0] = 0xFFFF0000;
        tints[1] = 0xFF00FF00;
        tints[2] = 0xFF0000FF;
        tints[3] = 0xFFAAAA00;

        graphics.imageMode(PConstants.CENTER);
    }

    @Override
    public void visit(Ship ship) {
        graphics.tint(tints[ship.getNumber() > 0 && ship.getNumber() < 5 ? ship.getNumber() -1: 0]);
        draw(ship, textures.getShipSprite());
        graphics.noTint();
    }

    @Override
    public void visit(Obstacle obstacle) {
        draw(obstacle, textures.getObstacleSprite());
    }

    @Override
    public void visit(Projectile projectile) {
        draw(projectile, textures.getProjectileSprite());
    }

    private void draw(Actor actor, PImage sprite) {
        if (drawHitboxes) {
            Rectangle2D test = actor.getShape().getBounds2D();
            graphics.fill(0xFFFF00FF);
            graphics.rect((float) test.getX(), (float) test.getY(), (float) test.getWidth(), (float) test.getHeight());
        }
        graphics.pushMatrix();
        graphics.translate(actor.getPosition().getX(), actor.getPosition().getY());
        graphics.rotate(actor.getRotation());
        graphics.image(sprite, 0, 0, actor.getWidth(), actor.getHeight());
        graphics.popMatrix();

    }

    public void setDrawHitBoxes(boolean drawHitboxes) {
        this.drawHitboxes = drawHitboxes;
    }
}
