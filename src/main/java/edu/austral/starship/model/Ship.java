package edu.austral.starship.model;

import edu.austral.starship.Observer;
import edu.austral.starship.Player;
import edu.austral.starship.Visitor;
import edu.austral.starship.base.vector.Vector2;

import java.util.function.Consumer;
import edu.austral.starship.Event;

public class Ship extends Actor {

    private Weapon weapon;
    private Integer number;

    public Ship(Vector2 position, Vector2 direction, Float speed, Integer width, Integer height) {
        super(position, direction, speed,width,height);
        this.weapon = new TripleWeapon(this);
    }

    @Override
    public void update(float timePassed) {
        super.update(timePassed);
        this.speed = 0f;
    }

    private void fire() {
        weapon.fire();
    }

    public void subscribe(Player player){
        this.observers.add(player);
        this.number = player.getNumber();
    }

    @Override
    public void collisionedWith(Actor collisionable) {
        collisionable.collisionedWith(this);
    }

    public void collisionedWith(Ship ship) {
        this.damage();
    }

    @Override
    public void collisionedWith(Obstacle obstacle) {
        this.damage();
    }

    @Override
    public void collisionedWith(Projectile projectile) {
        if(projectile.getShip() != this)
            this.damage();
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public static Consumer<Ship> accelerate() {
        return ship -> ship.setSpeed(+0.02f);
    }

    public static Consumer<Ship> decelerate() {
        return ship -> ship.setSpeed(-0.02f);
    }

    public static Consumer<Ship> rotClockWise() {
        return ship -> ship.rotateRight();
    }

    public static Consumer<Ship> rotCounterClockwise() {
        return ship -> ship.rotateLeft();
    }

    public static Consumer<Ship> shoot() {
        return ship -> ship.fire();
    }


    public void destroyedShip() {
        observers.forEach(x -> {
            x.update(this, Event.DESTROYED_SHIP);
        });
    }

    public void destroyedAsteroid() {
        observers.forEach(x -> {
            x.update(this,Event.DESTROYED_ASTEROID);
        });
    }

    public Integer getNumber() {
        return number;
    }
}
