package edu.austral.starship.model;

import edu.austral.starship.Visitor;
import edu.austral.starship.base.vector.Vector2;

import java.awt.*;

public class Projectile extends Actor {
    private Ship ship;

    public Projectile(Vector2 position, Vector2 direction, Float speed, Integer width, Integer height, Ship ship) {
        super(position, direction, speed, width, height);
        this.ship = ship;
    }

    @Override
    public void collisionedWith(Ship ship) {
        if (this.ship != ship) {
            destroy();
            this.ship.destroyedShip();
        }
    }

    @Override
    public void collisionedWith(Obstacle obstacle) {
        destroy();
        this.ship.destroyedAsteroid();
    }

    @Override
    public void collisionedWith(Projectile projectile) {

    }

    @Override
    public void collisionedWith(Actor collisionable) {
        collisionable.collisionedWith(this);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public Ship getShip() {
        return ship;
    }
}
