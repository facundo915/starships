package edu.austral.starship.model;

import edu.austral.starship.base.vector.Vector2;

public class BaseWeapon implements Weapon {
    private Ship ship;
    private Long lastFired;
    private Long firingRate; //Delay between shots in ms

    public BaseWeapon(Ship ship){
        lastFired = 0L;
        this.ship = ship;
        this.firingRate = 250L;
    }

    public void fire(){
        Long current = System.currentTimeMillis();
        if(lastFired < (current - firingRate)) {
            Vector2 pos = ship.getPosition().substract(Vector2.vectorFromModule(ship.getHeight()/2, ship.getRotation()).rotate((float) Math.PI / 2));
            Projectile p = new Projectile(pos, ship.getDirection(), 0.06f,10, 40,this.ship);
            ship.create(p);
            lastFired = current;
        }
    }
}
