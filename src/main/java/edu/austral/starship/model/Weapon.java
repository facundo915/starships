package edu.austral.starship.model;

import edu.austral.starship.base.vector.Vector2;

public interface Weapon {
     void fire();
}
