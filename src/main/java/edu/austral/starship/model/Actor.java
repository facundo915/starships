package edu.austral.starship.model;

import edu.austral.starship.CustomGameFramework;
import edu.austral.starship.Observer;
import edu.austral.starship.Visitor;
import edu.austral.starship.base.collision.Collisionable;
import edu.austral.starship.base.vector.Vector2;

import edu.austral.starship.Event;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

public abstract class Actor implements Collisionable<Actor> {
    protected Vector2 position;
    protected Vector2 direction;
    protected Float speed; // Should be units per ms
    protected Shape shape;
    protected List<Observer<Actor>> observers;

    public Actor(Vector2 position, Vector2 direction, Float speed, Integer width, Integer height) {
        Shape shape = new Rectangle((int) (position.getX() - width/2) ,(int) (position.getY() - height/2),width,height);
        this.position = position;
        this.direction = direction;
        this.speed = speed;
        this.shape = shape;
        observers = new ArrayList<>();
    }

    public void subscribe(Observer<Actor> observer) {
        observers.add(observer);
    }

    public void unsubscribe(Observer<Actor> observer) {
        observers.remove(observer);
    }

    public void destroy() {
        for (Observer ob : observers) {
            ob.update(this,Event.DESTROYED);
        }
    }
    protected void damage() {
        for (Observer ob : observers) {
            ob.update(this,Event.DAMAGE);
        }
    }

    public void create(Actor act){
        for(Observer ob: observers){
            ob.update(act,Event.CREATED);
        }
    }

    public void update(float timePassed) {
        Vector2 translation = direction.multiply(speed * timePassed);
        this.position = this.position.substract(translation);
        this.shape = translation.getTransform().createTransformedShape(shape);
        if(this.position.getX() > CustomGameFramework.getScreenW() + 200
                || this.position.getX() < -200
                || this.position.getY() > CustomGameFramework.getScreenH() + 200
                || this.position.getY() < -200){
            this.destroy();
        }
    }

    @Override
    public Shape getShape() {
        return shape;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public Vector2 getDirection() {
        return direction;
    }

    public void setDirection(Vector2 direction) {
        this.direction = direction;
    }

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    public abstract void collisionedWith(Ship ship);

    public abstract void collisionedWith(Obstacle obstacle);

    public abstract void collisionedWith(Projectile projectile);

    public abstract void accept(Visitor visitor);

    public Integer getWidth() {
        return shape.getBounds().width;
    }

    public Integer getHeight() {
        return shape.getBounds().height;
    }

    public float getRotation() {
        return direction.angle();
    }

    public void rotateRight(){
        this.direction = this.direction.rotate(0.04f);
    }
    public void rotateLeft(){
        this.direction = this.direction.rotate(-0.04f);
    }

}
