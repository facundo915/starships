package edu.austral.starship.model;

import edu.austral.starship.Visitor;
import edu.austral.starship.base.vector.Vector2;

import java.awt.*;

public class Obstacle extends Actor {
    public Obstacle(Vector2 position, Vector2 direction, Float speed, Integer width, Integer height) {
        super(position, direction, speed,width,height);
    }


    @Override
    public void collisionedWith(Ship ship) {
        this.destroy();
    }
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void collisionedWith(Obstacle obstacle) {
        this.destroy();
    }

    @Override
    public void collisionedWith(Projectile projectile) {
        this.destroy();
    }

    @Override
    public void collisionedWith(Actor collisionable) {
        collisionable.collisionedWith(this);
    }
}
