package edu.austral.starship;

import edu.austral.starship.model.Obstacle;
import edu.austral.starship.model.Projectile;
import edu.austral.starship.model.Ship;

public interface Visitor {
    public void visit(Ship ship);
    public void visit(Obstacle obstacle);
    public void visit(Projectile projectile);
}
