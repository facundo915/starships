package edu.austral.starship;

import edu.austral.starship.base.vector.Vector2;
import edu.austral.starship.model.Obstacle;

import java.util.Random;

public class AsteroidSpawner{
    private final CustomGameFramework customGameFramework;
    private final long delay;
    private final int minW;
    private final int minH;
    private final int maxW;
    private final int maxH;
    private final int gameWidth;
    private long milis;
    private final Random random = new Random();

    public AsteroidSpawner(CustomGameFramework customGameFramework,long delay, int minW, int minH, int maxW, int maxH, int gameWidth){
        this.customGameFramework = customGameFramework;
        this.delay = delay;
        this.minW = minW;
        this.minH = minH;
        this.maxW = maxW;
        this.maxH = maxH;
        this.gameWidth = gameWidth;
    }

    public AsteroidSpawner(CustomGameFramework customGameFramework,int gameWidth) {
        this.customGameFramework = customGameFramework;
        this.delay = 500;
        this.minW = 10;
        this.minH = 10;
        this.maxW = 100;
        this.maxH = 100;
        this.gameWidth = gameWidth;
    }

    public void update(){
        long current = System.currentTimeMillis();
        if(current > milis +  delay){
            milis = current;
            randomAsteroid();
        }
    }

    private void randomAsteroid(){
        Vector2 position = new Vector2(randomBetween(0,gameWidth),-maxH);
        Vector2 direction = new Vector2(0,-1);
        Obstacle a = new Obstacle(position,direction,0.01f,randomBetween(minW,maxW),randomBetween(minH,maxH));
        customGameFramework.update(a,Event.CREATED);
    }

    private int randomBetween(int floor,int ceiling){
        return random.nextInt(ceiling - floor) + floor;
    }
}
