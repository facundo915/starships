package edu.austral.starship;

import edu.austral.starship.model.Actor;
import edu.austral.starship.model.Ship;

import java.util.function.Consumer;

public class Player implements Observer<Actor>{
    private Ship ship;
    private KeyBindings keyBindings;
    private Integer number; //Ex: Player 1
    private Integer lives;
    private Integer score = 0;

    public Player(Ship ship, KeyBindings keyBindings, Integer number, Integer lives) {
        this.ship = ship;
        this.keyBindings = keyBindings;
        this.number = number;
        this.lives = lives;
        ship.subscribe(this);
    }

    public void onKey(Integer keyCode) {
        if (lives > 0) {
            Consumer<Ship> func = keyBindings.getFuncitonForKeyCode(keyCode);
            if(func != null)
                func.accept(ship);
        }
    }

    @Override
    public void update(Actor ship, Event event) {
        if(lives > 0) {
            switch (event){
                case DAMAGE: this.lives -= 1; break;
                case DESTROYED_SHIP: this.score += 100; break;
                case DESTROYED_ASTEROID: this.score += 1; break;
            }
            if (lives == 0) {
                ship.destroy();
                this.ship = null;
            }
        }
    }

    public boolean isAlive() {
        return ship != null;
    }

    public Integer getLives() {
        return lives;
    }

    public Integer getScore() {
        return score;
    }

    public Integer getNumber() {
        return number;
    }

    public Ship getShip() {
        return ship;
    }
}
