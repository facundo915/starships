package edu.austral.starship;

import edu.austral.starship.base.collision.CollisionEngine;
import edu.austral.starship.base.framework.GameFramework;
import edu.austral.starship.base.framework.ImageLoader;
import edu.austral.starship.base.framework.WindowSettings;
import edu.austral.starship.base.vector.Vector2;
import edu.austral.starship.model.Actor;
import edu.austral.starship.model.Ship;
import edu.austral.starship.view.DrawingVisitor;
import edu.austral.starship.view.HUD;
import edu.austral.starship.view.TexturePack;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.event.KeyEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class CustomGameFramework implements GameFramework,Observer<Actor> {
    private List<Actor> actors;
    private List<Player> players;
    private CollisionEngine<Actor> collisionEngine;
    private DrawingVisitor drawingVisitor;
    private TexturePack defaultTexturePack;
    private AsteroidSpawner asteroidSpawner;
    private HUD hud;
    private static final int screenW = 800;
    private static final int screenH = 600;

    @Override
    public void setup(WindowSettings windowsSettings, ImageLoader imageLoader) {
        windowsSettings
            .setSize(screenW, screenH);
        collisionEngine = new CollisionEngine<>();
        actors = new ArrayList<>();
        players = new ArrayList<>();
        PImage ship = imageLoader.load("src/main/resources/sprite/ship.png");
        PImage projectile = imageLoader.load("src/main/resources/sprite/shot.png");
        PImage obstacle = imageLoader.load("src/main/resources/sprite/asteroid.png");
        defaultTexturePack = new TexturePack(ship,projectile,obstacle);
        asteroidSpawner = new AsteroidSpawner(this,screenW);
        hud = new HUD(screenW,screenH);
    }

    @Override
    public void draw(PGraphics graphics, float timeSinceLastDraw, Set<Integer> keySet) {
        if(drawingVisitor == null){
            drawingVisitor = new DrawingVisitor(graphics, defaultTexturePack,false);
        }
        hud.draw(graphics);
        collisionEngine.checkCollisions(actors);
        for (Actor actor : actors) {
            actor.update(timeSinceLastDraw);
            actor.accept(drawingVisitor);
        }
        for (Integer key : keySet) {
            players.forEach(player -> {
                player.onKey(key);
            });
        }
        asteroidSpawner.update();

    }

    @Override
    public void keyPressed(KeyEvent event) {
        if(event.getKeyCode() == java.awt.event.KeyEvent.VK_F1){
            spawnPlayer(1,KeyBindings.def());
        }
        if(event.getKeyCode() == java.awt.event.KeyEvent.VK_F2){
            KeyBindings keyBindings = new KeyBindings();
            keyBindings.bindKey(java.awt.event.KeyEvent.VK_UP,Ship.accelerate());
            keyBindings.bindKey(java.awt.event.KeyEvent.VK_DOWN,Ship.decelerate());
            keyBindings.bindKey(java.awt.event.KeyEvent.VK_LEFT,Ship.rotCounterClockwise());
            keyBindings.bindKey(java.awt.event.KeyEvent.VK_RIGHT,Ship.rotClockWise());
            keyBindings.bindKey(java.awt.event.KeyEvent.VK_CONTROL,Ship.shoot());
            spawnPlayer(2,keyBindings);
        }
        if(event.getKeyCode() == java.awt.event.KeyEvent.VK_F3){
            spawnPlayer(3,KeyBindings.def());
        }
        if(event.getKeyCode() == java.awt.event.KeyEvent.VK_F4){
            spawnPlayer(4,KeyBindings.def());
        }
    }

    @Override
    public void keyReleased(KeyEvent event) {

    }

    @Override
    public void update(Actor actor, Event event) {
        this.actors = new ArrayList<>(actors);
        switch(event){
            case CREATED: this.actors.add(actor); actor.subscribe(this); break;
            case DESTROYED: this.actors.remove(actor); break;
        }
    }

    private void spawnPlayer(Integer number,KeyBindings bindings){
        Optional<Player> oldP = players.stream().filter(x -> x.getNumber().equals(number)).findAny();
        if(oldP.isPresent()){
            players = new ArrayList<>(players);
            actors = new ArrayList<>(actors);
            players.remove(oldP.get());
            hud.removePlayer(oldP.get());
            actors.remove(oldP.get().getShip());
        }
        Ship ship = new Ship(new Vector2(screenW/4 * number + 10,(float) (screenH * 0.8)), new Vector2(0,1),0f,50,50);
        actors.add(ship);
        Player p = new Player(ship,bindings,number,5);
        players.add(p);
        ship.subscribe(this);
        hud.addPlayer(p);
    }

    public static int getScreenW() {
        return screenW;
    }

    public static int getScreenH() {
        return screenH;
    }
}
